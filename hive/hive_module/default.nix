{ config, lib, pkgs, ...}:
let
  cfg = config.services.hive;
  hiveConf = import ./conf.nix { hive = cfg; pkgs = pkgs; };
in
with lib;
{

  options.services.hive = {
    site = mkOption {
      default = {};
      example = {
        "hive.metastore.db.type" = "postgresql";
      };
      description = "hive-site.xml definition";
    };
  config = mkMerge [
    (mkIf cfg.hive.enabled {};)
  ];
