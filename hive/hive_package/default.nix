{ stdenv, fetchzip, makeWrapper, hadoop_3_1 }:

stdenv.mkDerivation rec {
  name = "hive-${version}";
  version = "3.1.2";

  src = fetchzip {
    url = "http://mirrors.koehn.com/apache/hive/hive-3.1.2/apache-hive-3.1.2-bin.tar.gz";
    sha256 = "1g4y3378y2mwwlmk5hs1695ax154alnq648hn60zi81i83hbxy5q";
  };

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
  mkdir -p $out/bin
  mkdir -p $out/lib
  mkdir -p $out/conf
  mkdir -p $out/jdbc
  cp -r  $src/bin/* $out/bin
  cp -r  $src/lib/* $out/lib
  cp -r  $src/conf/* $out/conf
  cp -r  $src/jdbc/* $out/jdbc
   wrapProgram $out/bin/hive \
      --set HADOOP_PREFIX ${hadoop_3_1}
  ls -la
  ''; 
}
